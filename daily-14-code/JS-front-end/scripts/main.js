console.log("Started main.js for Daily 14");

// Event trigger - react to button clicks
document.getElementById("submit_button").onmouseup = getFormInfo;
document.getElementById("reset_button").onmouseup = clearForm;

// Event handler - Print statement
function getFormInfo() {
    console.log("Entered getFormInfo()");
    
    // Clear previous response area
    document.getElementById("response-div1").innerHTML = "";

    // Get request info from form
    var url  = document.getElementById("select-url").value; 
    var port = document.getElementById("port").value;
    var key  = document.getElementById("key").value;
    var body = document.getElementById("message-body").value;

    // Get request type from radio buttons
    var methods = document.getElementsByName("bsr-radios");
    for (var i = 0; i < methods.length; i++) {
        if (methods[i].checked)
        var method = methods[i].value;
    }
    
    // Make call to countries predictor API
    if (url == "URL" || port == "") {
        document.getElementById("response-div1").innerHTML = "Please select a URL and enter a port number before making a request.";
    } else if (method != "GET" && !document.getElementById("message-body-checkbox").checked) {
        document.getElementById("response-div1").innerHTML = "You need to send a message body with a PUT, POST, or DELETE request to the /movies/ endpoint.";
    } else {
        makeMovieCall(url, port, key, method, body);
    }
    
    console.log("Exited getFormInfo()");
}

function makeMovieCall(url, port, key, method, body) {
    console.log("Entered makeMovieCall()");
    
    // Set up URL
    var xhr = new XMLHttpRequest();
    var full_url = url + ":" + port + /movies/;
    if (document.getElementById("key-checkbox").checked) {
        full_url += key;
    }
    
    // Make request
    xhr.open(method, full_url, true);
    
    console.log(body);
    
    if (document.getElementById("message-body-checkbox").checked) {
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(body);  
    } else {
        xhr.send(null);
    }
     
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updatePageWithResponse(name, xhr.responseText, method);
    }
    
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }
    
    console.log("Exited makeMovieCall()");
}

function updatePageWithResponse(name, response_text, method) {
    console.log("Entered updatePageWithResponse()");
    
    var response_json = JSON.parse(response_text);
    var line1 = document.getElementById("response-div1");
    
    var formats = document.getElementsByName("format-radios");
    for (var i = 0; i < formats.length; i++) {
        if (formats[i].checked)
        var format = formats[i].value;
    }
    
    if (response_json == undefined || response_json.length == 0) {
        line1.innerHTML = "<h3><b>Cannot find response body</b></h3>";
    } else {
        if (format == "raw") {
            line1.innerHTML = '<pre id="json"></pre>';
            document.getElementById("json").textContent = response_text;
        } else if (format == "pretty") {
            line1.innerHTML = '<pre id="json"></pre>';
            document.getElementById("json").textContent = JSON.stringify(response_json, null, 2);
        } else if (format == "format") {
            line1.innerHTML = "<h3><b>Result: " + response_json['result'] + "</b></h3>";
            if (method == "POST") {
                line1.innerHTML += "<h4>ID: " + response_json['id'] + "</h4>";
            } else if (method == "GET") {
                if (document.getElementById("key-checkbox").checked) {
                    line1.innerHTML += "<h4>ID: " + response_json['id'] + "</h4>";
                    line1.innerHTML += "<h4>Title: " + response_json['title'] + "</h4>";
                    line1.innerHTML += "<h4>Genres: " + response_json['genres'] + "</h4>";
                } else {
                    line1.innerHTML += "<ul>";
                    for (var i = 0; i < response_json['movies'].length; i++) {
                        line1.innerHTML += "<li>" + response_json['movies'][i]['id'] + ": " + response_json['movies'][i]['title'] + " [" + response_json['movies'][i]['genres'] + "]</li>";
                    }
                    line1.innerHTML += "</ul>";
                }
            }
        }
    }
    
    console.log("Exited updatePageWithResponse()");
}

// Event handler - Clear form
function clearForm() {
    console.log("Entered clearForm()");

    // Clear form
    document.getElementById("select-url").value = "URL";
    document.getElementById("port").value = "";
    document.getElementById("key").value = "";
    document.getElementById("message-body").value = "";
    
    var methods = document.getElementsByName("bsr-radios");
    for (var i = 0; i < methods.length; i++) {
        methods[i].checked = false;
    }
    
    var formats = document.getElementsByName("format-radios");
    for (var i = 0; i < formats.length; i++) {
        formats[i].checked = false;
    }
    
    document.getElementById("key-checkbox").checked = false;
    document.getElementById("message-body-checkbox").checked = false;

    // Clear response area
    document.getElementById("response-div1").innerHTML = "";
    
    console.log("Exited clearForm()");
}

console.log("Started main.js for Daily 13");

// Event trigger - react to button clicks
document.getElementById("submit_button").onmouseup = getFormInfo;
document.getElementById("reset_button").onmouseup = clearForm;

// Event handler - Print statement
function getFormInfo() {
    console.log("Entered getFormInfo()");
    
    // Clear previous response area
    document.getElementById("response-line1").innerHTML = "";
    document.getElementById("response-line2").innerHTML = "";
    document.getElementById("response-line3").innerHTML = "";
    document.getElementById("response-div2").innerHTML = "";

    // Get name from input text
    var name = document.getElementById("input-text").value;

    // Get value of radio buttons
    var radios = document.getElementsByName("animal-radios");
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked)
        var radio = radios[i].value;
    }
    
    // Make call to countries predictor API
    if (name != "") {
        makeCountryCall(name);
        
        if (radio == "cat") {
            makeCatCall();
        } else if (radio == "dog") {
            makeDogCall();
        }
    } else {
        document.getElementById("response-line1").innerHTML = "Please enter a name first.";
    }
    
    console.log("Exited getFormInfo()");
}

function makeCountryCall(name) {
    console.log("Entered makeCountryCall()");
    
    // Set up URL
    var xhr = new XMLHttpRequest();
    var url = "https://api.nationalize.io/?name=" + name;
    xhr.open("GET", url, true);
    
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updateCountryWithResponse(name, xhr.responseText);
    }
    
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }
    
    xhr.send(null);
    
    console.log("Exited makeCountryCall()");
}

function updateCountryWithResponse(name, response_text) {
    console.log("Entered updateCountryWithResponse()");
    
    var response_json = JSON.parse(response_text);
    var line1 = document.getElementById("response-line1");
    
    if (response_json['country'] == undefined || response_json['country'].length == 0) {
        line1.innerHTML = "Hmmmm, " + name + "? That's quite an unusual name. You aren't from around here, are you?"
    } else {
        line1.innerHTML = name + ", it's likely that your country code is " + response_json['country'][0]['country_id'] + ".";
        makeCountryInfoCall(response_json['country'][0]['country_id']);
    }
    
    console.log("Exited updateCountryWithResponse()");
}

function makeCountryInfoCall(country_code) {
    console.log("Entered makeCountryInfoCall()");
    
    var xhr = new XMLHttpRequest();
    var url = "https://restcountries.eu/rest/v2/alpha/" + country_code;
    xhr.open("GET", url, true);
    
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updateCapitalWithResponse(xhr.responseText);
    }
    
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }
    
    xhr.send(null);
    
    console.log("Exited makeCountryInfoCall()");
}

function updateCapitalWithResponse(response_text) {
    console.log("Entered updateCapitalWithResponse()");
    
    var response_json = JSON.parse(response_text);
    var line2 = document.getElementById("response-line2");
    var line3 = document.getElementById("response-line3");
    
    line2.innerHTML = "So I think you're from " + response_json['name'] + ".";
    line3.innerHTML = "Did you know that the capital of " + response_json['name'] + " is " + response_json['capital'] + "? Well, I guess you knew that since you're likely from " + response_json['name'] + ". But if you didn't know, now you know!";
        
    console.log("Exited updateCapitalWithResponse()");
}

function makeCatCall() {
    console.log("Entered makeCatCall()");
    
    // Set up URL
    var xhr = new XMLHttpRequest();
    var url = "https://cat-fact.herokuapp.com/facts";
    xhr.open("GET", url, true);
    
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updateCatWithResponse(xhr.responseText);
    }
    
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }
    
    xhr.send(null);
    
    console.log("Exited makeCatCall()");
}

function updateCatWithResponse(response_text) {
    console.log("Entered updateCatWithResponse()");
    
    var response_json = JSON.parse(response_text);
    var random_number = Math.floor(Math.random() * 100);
    
    var cat_label = document.createElement("label");
    cat_label.setAttribute("id", "cat-label");
    var cat_text = document.createTextNode("Here's a fun fact about cats:");
    cat_label.appendChild(cat_text);
    document.getElementById("response-div2").appendChild(cat_label);
    
    var cat_para = document.createElement("P");
    cat_para.setAttribute("id", "cat-fact");
    var cat_fact = document.createTextNode(response_json['all'][random_number]['text']);
    cat_para.appendChild(cat_fact);
    document.getElementById("response-div2").appendChild(cat_para);
    
    console.log("Exited updateCatWithResponse()");
}

function makeDogCall() {
    console.log("Entered makeDogCall()");
    
    // Set up URL
    var xhr = new XMLHttpRequest();
    var url = "https://dog.ceo/api/breeds/image/random";
    xhr.open("GET", url, true);
    
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updateDogWithResponse(xhr.responseText);
    }
    
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }
    
    xhr.send(null);
    
    console.log("Exited makeDogCall()");
}

function updateDogWithResponse(response_text) {
    console.log("Entered updateDogWithResponse()");
    
    var response_json = JSON.parse(response_text);
    
    var dog_label = document.createElement("label");
    dog_label.setAttribute("id", "dog-label");
    var dog_text = document.createTextNode("Here's a picture of a dog:");
    dog_label.appendChild(dog_text);
    document.getElementById("response-div2").appendChild(dog_label);
    
    
    var dog_para = document.createElement("P");
    dog_para.setAttribute("id", "dog-pic");
    var dog_pic = document.createElement("img");
    dog_pic.src = response_json['message'];
    dog_para.appendChild(dog_pic);
    document.getElementById("response-div2").appendChild(dog_para);
    
    console.log("Exited updateDogWithResponse()");
}

// Event handler - Clear form
function clearForm() {
    console.log("Entered clearForm()");

    // Clear form
    document.getElementById("input-text").value = "";
    var radios = document.getElementsByName("animal-radios");

    for (var i = 0; i < radios.length; i++) {
        radios[i].checked = false;
    }

    document.getElementById("response-line1").innerHTML = "";
    document.getElementById("response-line2").innerHTML = "";
    document.getElementById("response-line3").innerHTML = "";
    document.getElementById("response-div2").innerHTML = "";
    
    console.log("Exited clearForm()");
}
